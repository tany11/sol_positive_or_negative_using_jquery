﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_Positive_Negative_Number_Jquery
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCheck_Click(object sender, EventArgs e)
        {
            // read Value from TextBox
            int value = Convert.ToInt32(txtValue.Text);

            if (value >= 0)
            {
                divMarking.Attributes.Add("class", "divBorder positiveMarking");
            }
            else
            {
                divMarking.Attributes.Add("class", "divBorder negativeMarking");
            }
        }
    }
}
