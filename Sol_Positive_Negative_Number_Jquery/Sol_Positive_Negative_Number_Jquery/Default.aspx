﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Sol_Positive_Negative_Number_Jquery.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="jquery.js"></script>
    <script src="Scripts/jquery-3.1.1.min.js"></script>
    <style>
        .divBorder{
            border-color:black;
            border-radius:15px;
            border-style:dotted;
            height:20px;
            width:20px;
        }


        .negativeMarking.divBorder{
            background-color: red;
        }

        .positiveMarking.divBorder{
            background-color:green;
        }
    </style>
    <script>
$(document).ready(function(){
 $("#btnCheck_Click").click(function(){
     var num = $("#txtValue").val();
        if (jQuery.isNumeric(num) == false) {
            alert('Please enter numeric value');
            $('#txtValue').val('');
           
            $('#txtValue').focus();
        }
        else if (num >= 0) {
            remarks = num + " is a Positive Number.";
           
             else {
                remarks = num + " is a Negative Number.";
               
            }
        });
       


});
        </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
             <asp:TextBox ID="txtValue" runat="server"></asp:TextBox>
            <asp:Button ID="btnCheck" runat="server" Text="Check" OnClick="btnCheck_Click" />

            <div id="divMarking" runat="server"></div>

        </div>
    </form>
</body>
</html>
